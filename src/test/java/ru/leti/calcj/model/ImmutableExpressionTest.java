package ru.leti.calcj.model;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.function.DoubleBinaryOperator;

import static org.assertj.core.api.Assertions.assertThat;

@DisplayName("Tests for ImmutableExpression")
class ImmutableExpressionTest {
    private static final DoubleBinaryOperator PLUS = Double::sum;

    @Test
    @DisplayName("Expression should set an operator correctly")
    void testSetOperator() {
        // Given
        var before = new ImmutableExpression("12.34", "", null);
        // When
        var after = before.setOperator(PLUS);
        // Then
        assertThat(after.value()).isEqualTo("");
        assertThat(after.memValue()).isEqualTo("12.34");
        assertThat(after.operator()).isEqualTo(PLUS);
        assertThat(after.isDotPlaced()).isFalse();
    }

    @Test
    @DisplayName("Expression should set an operator correctly and calculate, if there is value stored")
    void testSetOperatorWhenHasMemValue() {
        // Given
        var before = new ImmutableExpression("34.56", "12.34", PLUS);
        // When
        var after = before.setOperator(PLUS);
        // Then
        assertThat(after.value()).isEqualTo("");
        assertThat(after.memValue()).isEqualTo("46.9");
        assertThat(after.operator()).isEqualTo(PLUS);
        assertThat(after.isDotPlaced()).isFalse();
    }

    @Test
    @DisplayName("Expression should clear value correctly")
    void testClear() {
        // Given
        var before = new ImmutableExpression("12.34", "", null);
        // When
        var after = before.clear();
        // Then
        assertThat(after.value()).isEqualTo("");
        assertThat(after.isDotPlaced()).isFalse();
    }

    @Test
    @DisplayName("Expression should clear value and stored value correctly")
    void testClearAll() {
        // Given
        var before = new ImmutableExpression("34.56", "12.34", PLUS);
        // When
        var after = before.clearAll();
        // Then
        assertThat(after.value()).isEqualTo("");
        assertThat(after.memValue()).isEqualTo("");
        assertThat(after.operator()).isNull();
        assertThat(after.isDotPlaced()).isFalse();
    }

    @Test
    @DisplayName("Expression should append number correctly")
    void testAppend() {
        // Given
        var before = new ImmutableExpression();
        // When
        var after = before.append('1');
        // Then
        assertThat(after.value()).isEqualTo("1");
        assertThat(after.isDotPlaced()).isFalse();
    }

    @Test
    @DisplayName("Expression should place dot correctly")
    void testPlaceDot() {
        // Given
        var before = new ImmutableExpression("12", "", null);
        // When
        var after = before.placeDot();
        // Then
        assertThat(after.value()).isEqualTo("12.");
        assertThat(after.isDotPlaced()).isTrue();
    }

    @Test
    @DisplayName("Expression should calculate result correctly")
    void testCalculate() {
        // Given
        var before = new ImmutableExpression("34.56", "12.34", PLUS);
        // When
        var after = before.calculate();
        // Then
        assertThat(after.value()).isEqualTo("46.9");
        assertThat(after.memValue()).isEqualTo("");
        assertThat(after.operator()).isNull();
        assertThat(after.isDotPlaced()).isTrue();
    }

    @Test
    @DisplayName("Expression should calculate result and set dot flag correctly")
    void testCalculateNoDot() {
        // Given
        var before = new ImmutableExpression("34.66", "12.34", PLUS);
        // When
        var after = before.calculate();
        // Then
        assertThat(after.value()).isEqualTo("47");
        assertThat(after.memValue()).isEqualTo("");
        assertThat(after.operator()).isNull();
        assertThat(after.isDotPlaced()).isFalse();
    }
}