package ru.leti.calcj.model;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.within;

@DisplayName("Tests for BinaryOperatorMap")
public class BinaryOperatorMapTest {
    @Test
    @DisplayName("BinaryOperatorMap should return Optional.empty for invalid characters")
    public void testFromCharInvalidCharacters() {
        List<Character> validChars = List.of('+', '-', '*', '/');
        Stream.iterate(Character.MIN_VALUE, c -> c < Character.MAX_VALUE, c -> (char) (c + 1))
                .filter(c -> !validChars.contains(c))
                .forEach(c -> assertThat(BinaryOperatorMap.fromChar(c)).isEmpty());
    }

    @Test
    @DisplayName("BinaryOperatorMap should return operation")
    public void testOperation() {
        assertThat(BinaryOperatorMap.fromChar('+'))
                .map(op -> op.applyAsDouble(3.0, 2.0))
                .hasValueSatisfying(d -> assertThat(d).isCloseTo(5.0, within(1E-6)));
        assertThat(BinaryOperatorMap.fromChar('-'))
                .map(op -> op.applyAsDouble(3.0, 2.0))
                .hasValueSatisfying(d -> assertThat(d).isCloseTo(1.0, within(1E-6)));
        assertThat(BinaryOperatorMap.fromChar('*'))
                .map(op -> op.applyAsDouble(3.0, 2.0))
                .hasValueSatisfying(d -> assertThat(d).isCloseTo(6.0, within(1E-6)));
        assertThat(BinaryOperatorMap.fromChar('/'))
                .map(op -> op.applyAsDouble(3.0, 2.0))
                .hasValueSatisfying(d -> assertThat(d).isCloseTo(1.5, within(1E-6)));
    }
}
