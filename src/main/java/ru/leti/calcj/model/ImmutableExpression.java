package ru.leti.calcj.model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.DecimalFormat;
import java.util.Objects;
import java.util.Optional;
import java.util.function.DoubleBinaryOperator;

public record ImmutableExpression(String value, String memValue, DoubleBinaryOperator operator) {
    private static final Logger logger = LoggerFactory.getLogger(ImmutableExpression.class);

    public ImmutableExpression() {
        this("", "", null);
    }

    public boolean isDotPlaced() {
        return value.indexOf('.') != -1;
    }

    public ImmutableExpression append(char charAt) {
        return new ImmutableExpression(
                value + charAt,
                memValue,
                operator
        );
    }

    public ImmutableExpression placeDot() {
        return isDotPlaced() ? this : new ImmutableExpression(
                value + '.',
                memValue,
                operator
        );
    }

    public ImmutableExpression setOperator(DoubleBinaryOperator operator) {
        String newMemValue = Objects.equals(memValue, "") ? value : calculate().value;
        return new ImmutableExpression(
                "",
                newMemValue,
                operator
        );
    }

    public ImmutableExpression calculate() {
        double lhs = Double.parseDouble(memValue);
        double rhs = Double.parseDouble(value);

        double res = Optional.ofNullable(operator).map(o -> o.applyAsDouble(lhs, rhs)).orElseThrow();
        String tmpValue = new DecimalFormat("#.##########").format(res);
        return new ImmutableExpression(
                tmpValue,
                "",
                null
        );
    }

    public ImmutableExpression clearAll() {
        return new ImmutableExpression();
    }

    public ImmutableExpression clear() {
        return new ImmutableExpression(
                "",
                memValue,
                operator
        );
    }
}
