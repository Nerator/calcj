package ru.leti.calcj.model;

import java.util.Map;
import java.util.Optional;
import java.util.function.DoubleBinaryOperator;

public class BinaryOperatorMap {
    private BinaryOperatorMap() {
    }

    private static final Map<Character, DoubleBinaryOperator> binaryOperatorMap = Map.ofEntries(
            Map.entry('+', Double::sum),
            Map.entry('-', (a, b) -> a - b),
            Map.entry('*', (a, b) -> a * b),
            Map.entry('/', (a, b) -> a / b)
    );

    public static Optional<DoubleBinaryOperator> fromChar(char c) {
        return Optional.ofNullable(binaryOperatorMap.get(c));
    }
}
