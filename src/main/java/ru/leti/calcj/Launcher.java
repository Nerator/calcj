package ru.leti.calcj;

/**
 * Точка входа в программу - запускает основное приложение JavaFX
 * Необходим для формирования нормального пакета без использования модулей
 */
public class Launcher {
    public static void main(String[] args) {
        CalcJ.main(args);
    }
}
