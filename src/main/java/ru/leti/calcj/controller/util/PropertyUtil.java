package ru.leti.calcj.controller.util;

import javafx.beans.value.WritableValue;

import java.util.function.UnaryOperator;

public class PropertyUtil {
    private PropertyUtil() {
    }

    public static <T> void update(WritableValue<T> writableValue, UnaryOperator<T> operator) {
        writableValue.setValue(operator.apply(writableValue.getValue()));
    }
}
