package ru.leti.calcj.controller;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.leti.calcj.controller.util.PropertyUtil;
import ru.leti.calcj.model.BinaryOperatorMap;
import ru.leti.calcj.model.ImmutableExpression;

/**
 * Контроллер для главного окна приложения
 * Содержит обработчики событий для элементов интерфейса и связывает их с логикой
 */
public class CalcJController {
    private final Logger logger = LoggerFactory.getLogger(CalcJController.class);

    private final ObjectProperty<ImmutableExpression> immutableExpressionObjectProperty =
            new SimpleObjectProperty<>(new ImmutableExpression());

    @FXML
    private TextField calcField;

    /**
     * Инициализация контроллера
     * Здесь связывается содержимое текстового поля с значением выражения
     */
    @FXML
    private void initialize() {
        logger.info("Initializing");
        immutableExpressionObjectProperty.addListener((observable, oldValue, newValue) ->
                calcField.textProperty().setValue(observable.getValue().value()));
    }

    /**
     * Действие при нажатии на кнопку с цифрой
     *
     * @param ae событие
     */
    @FXML
    private void numButtonAction(ActionEvent ae) {
        logger.info("Num button press: " + ae.getSource());
        Button src = (Button) ae.getSource();
        PropertyUtil.update(immutableExpressionObjectProperty, expr -> expr.append(src.getText().charAt(0)));
    }

    /**
     * Действие при нажатии на кнопку "." (десятичный разделитель)
     */
    @FXML
    private void dotButtonAction() {
        PropertyUtil.update(immutableExpressionObjectProperty, ImmutableExpression::placeDot);
    }

    /**
     * Действие при нажатии на кнопку с оператором (+, -, *, /)
     *
     * @param ae событие
     */
    @FXML
    private void opButtonAction(ActionEvent ae) {
        logger.info("Op button press: " + ae.getSource());
        Button src = (Button) ae.getSource();
        char opChar = src.getText().charAt(0);
        PropertyUtil.update(immutableExpressionObjectProperty, expr ->
                expr.setOperator(BinaryOperatorMap.fromChar(opChar)
                        .orElseThrow(() -> new IllegalStateException("Should not happen: got character " + opChar))));
    }

    /**
     * Действие при нажатии на кнопку =
     */
    @FXML
    private void eqButtonAction() {
        PropertyUtil.update(immutableExpressionObjectProperty, ImmutableExpression::calculate);
    }

    /**
     * Действие при нажатии на кнопку AC
     */
    @FXML
    private void acButtonAction() {
        PropertyUtil.update(immutableExpressionObjectProperty, ImmutableExpression::clearAll);
    }

    /**
     * Действие при нажатии на кнопку C
     */
    @FXML
    private void cButtonAction() {
        PropertyUtil.update(immutableExpressionObjectProperty, ImmutableExpression::clear);
    }
}
