package ru.leti.calcj;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * Основное окно приложения
 * Класс загружает разметку интерфейса из FXML файла
 */
public class CalcJ extends Application {
    @Override
    public void start(Stage stage) {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/ru/leti/calcj/view/CalcJ.fxml"));

        try {
            Scene scene = new Scene(loader.load(), 640, 480);
            stage.setTitle("CalcJ");
            stage.setScene(scene);
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch();
    }
}