package ru.leti.calcj;

import javafx.stage.Stage;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.testfx.framework.junit5.ApplicationExtension;
import org.testfx.framework.junit5.ApplicationTest;
import org.testfx.matcher.control.LabeledMatchers;
import org.testfx.matcher.control.TextInputControlMatchers;

import static org.testfx.api.FxAssert.verifyThat;

@ExtendWith(ApplicationExtension.class)
public class CalcJTest extends ApplicationTest {
    @BeforeAll
    public static void setupAll() {
        System.setProperty("testfx.robot", "glass");
        System.setProperty("testfx.headless", "true");
        System.setProperty("prism.order", "sw");
        System.setProperty("prism.text", "t2k");
        System.setProperty("java.awt.headless", "true");
    }

    @Override
    public void start(Stage stage) {
        new CalcJ().start(stage);
    }

    @Test
    @DisplayName("Initial structure should be good")
    public void testInitialStructure() {
        verifyThat("#zero", LabeledMatchers.hasText("0"));
        verifyThat("#one", LabeledMatchers.hasText("1"));
        verifyThat("#two", LabeledMatchers.hasText("2"));
        verifyThat("#three", LabeledMatchers.hasText("3"));
        verifyThat("#four", LabeledMatchers.hasText("4"));
        verifyThat("#five", LabeledMatchers.hasText("5"));
        verifyThat("#six", LabeledMatchers.hasText("6"));
        verifyThat("#seven", LabeledMatchers.hasText("7"));
        verifyThat("#eight", LabeledMatchers.hasText("8"));
        verifyThat("#nine", LabeledMatchers.hasText("9"));
        verifyThat("#dot", LabeledMatchers.hasText("."));

        verifyThat("#plus", LabeledMatchers.hasText("+"));
        verifyThat("#minus", LabeledMatchers.hasText("-"));
        verifyThat("#mult", LabeledMatchers.hasText("*"));
        verifyThat("#div", LabeledMatchers.hasText("/"));
        verifyThat("#eq", LabeledMatchers.hasText("="));

        verifyThat("#c", LabeledMatchers.hasText("C"));
        verifyThat("#ac", LabeledMatchers.hasText("AC"));

        verifyThat("#calc", TextInputControlMatchers.hasText(""));
    }

    @Test
    @DisplayName("Numbers should be entered correctly")
    public void testNumberEntry() {
        clickOn("#one");
        verifyThat("#calc", TextInputControlMatchers.hasText("1"));
        clickOn("#two");
        verifyThat("#calc", TextInputControlMatchers.hasText("12"));
    }

    @Test
    @DisplayName("Dot should be placed only once")
    public void testDotOnlyOnce() {
        clickOn("#one");
        verifyThat("#calc", TextInputControlMatchers.hasText("1"));
        clickOn("#dot");
        verifyThat("#calc", TextInputControlMatchers.hasText("1."));
        clickOn("#dot");
        verifyThat("#calc", TextInputControlMatchers.hasText("1."));
        clickOn("#two");
        verifyThat("#calc", TextInputControlMatchers.hasText("1.2"));
        clickOn("#dot");
        verifyThat("#calc", TextInputControlMatchers.hasText("1.2"));
    }

    @Test
    @DisplayName("Sample operation should be performed")
    public void testSampleOperation() {
        clickOn("#one");
        verifyThat("#calc", TextInputControlMatchers.hasText("1"));
        clickOn("#plus");
        verifyThat("#calc", TextInputControlMatchers.hasText(""));
        clickOn("#two");
        verifyThat("#calc", TextInputControlMatchers.hasText("2"));
        clickOn("#eq");
        verifyThat("#calc", TextInputControlMatchers.hasText("3"));
    }
}
